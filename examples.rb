require_relative 'perceptron'

feature_set = [[0,1,0],[0,0,1],[1,0,0],[1,1,0],[1,1,1]]
feature_labels = [1,0,0,1,1]
seed = 24
learning_rate = 0.05
epochs = 20000

nn = Perceptron.new(feature_set, feature_labels, seed, learning_rate, epochs)
nn.learn()
nn.predict([0,1,0])
nn.error
