module VectorMath
    
    # Applies the Sigmoid function to a numeric value and returns the result
    def sigmoid(x)
        1 / (1 + (Math::E ** (-x)))
    end
    
    # Applies the derivative of the Sigmoid function to a numeric value and returns the result
    def sigmoid_derivative(x)
        sigmoid(x) * (1 - sigmoid(x))
    end
    
    # Returns the inner dot product of two arrays
    # Only supports single dimensionality
    # TODO: Support nested arrays and casting
    def dot_product(a, b)
        a.zip(b).map{ |x, y| x * y }.inject(:+)
    end
    
    # Performs an arithmetic function (+, -, *) stepwise on two arrays (Matrix calculation)
    # The function to be performed is passed as a symbol eg. :+
    # Division (:/) is not supported as Ruby will attempt to do integer division. You need to convert to floats first.
    # matrix_arithmetic([1, 2], [3, 4], :-) will return [-2, -2]
    def matrix_arithmetic(a, b, symbolic_operator)
        a.zip(b).map { |x, y| x.send(symbolic_operator, y) }
    end

    # Transposes the first element of each sub-array into the first column of a new two dimensional array (data frame)
    # Rinse and repeat for every element of the the respective sub-arrays
    def transpose(array)
        return_value = Array.new
        for i in 1..array.first.size do
            return_value << array.map { |sub_array| sub_array[i - 1] }
        end
        return_value
    end
    
end
