require_relative 'vector_math'

class Perceptron

    include VectorMath
    
    attr_accessor :error, :bias, :weights, :epochs, :seed

    def initialize(feature_set, feature_labels, seed, learning_rate, epochs)  
        Kernel.srand(seed) 
        @feature_set = feature_set  
        @feature_labels = feature_labels
        @learning_rate = learning_rate
        @epochs = epochs
        @weights = []
        @feature_set.first.size.times { @weights << rand() }
        @bias = rand()
        @error = nil
    end

    def learn(quiet = false)
        @epochs.times do
    
            # Feedforward step1
            # Find the dot product of the input and the weight vector and add bias to it.
            xw = @feature_set.map { |instance| dot_product(instance, @weights) + @bias } 
        
            # Feedforward step2
            # Pass the dot product through the sigmoid activation function
            # This completes the feed forward part of our algorithm.
            z = xw.map { |output|  sigmoid(output) }
        
            # Backpropagation step 1
            # Find the error by subtracting the actuals from the predicted values
            error = matrix_arithmetic(z, @feature_labels, :-)
            
            # Print the error
            @error = error.inject(:+)
            unless quiet
                print 'Error: '
                p @error
            end
        
            # Backpropagation step 2
            # Get the derivative of the step function
            dcost_dpred = error
            dpred_dz = z.map { |instance| sigmoid_derivative(instance) }
            
            # Multiply the Error by the derivative sigmoid vector
            z_delta = matrix_arithmetic(dcost_dpred, dpred_dz, :*)
            
            # Multiply the "z_derivative" by the inputs and multiply by the learning rate
            # Subtract the resulting vector from the current set of weights
            inputs = transpose(@feature_set)
            inputs = inputs.map { |array| dot_product(array, z_delta) }.map { |element| element * @learning_rate }
            @weights = matrix_arithmetic(@weights, inputs, :-)
            
            # Finally, we multiply the learning rate with the derivative to increase the speed of convergence.
            z_delta.each do |num|
                @bias -= @learning_rate * num
            end
        end
    end

    # Record in the form [1, 0, 1]
    # Weights in the form [0.9, 0.8, 0.2]
    # Values closer to 1 indicate a true(1) prediction
    # Values closer to 0 indicate a false(0) prediction
    def predict(record)
        sigmoid(dot_product(record, @weights) + @bias)
    end

    def predict_binary(record)
        predict(record) >= 0.5 ? 1 : 0
    end
    
end
