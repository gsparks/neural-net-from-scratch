require_relative '../perceptron'
require 'test/unit'

class TestPerceptron < Test::Unit::TestCase

  def setup()
    feature_set = [[0,1,0],[0,0,1],[1,0,0],[1,1,0],[1,1,1]]
    feature_labels = [1,0,0,1,1]
    seed = 24
    learning_rate = 0.05
    epochs = 20000
    Perceptron.new(feature_set, feature_labels, seed, learning_rate, epochs)
  end

  def test_sigmoid
    nn = setup()
    assert_equal(0.9933071490757153, nn.sigmoid(5) )
  end

  def test_sigmoid_derivative
    nn = setup()
    assert_equal(0.006648056670790033, nn.sigmoid_derivative(5) )
  end

  def test_matrix_arithmetic
    nn = setup()
    assert_equal([4, 6], nn.matrix_arithmetic([1, 2], [3, 4], :+) )
    assert_equal([-2, -2], nn.matrix_arithmetic([1, 2], [3, 4], :-) )
    assert_equal([3, 8], nn.matrix_arithmetic([1, 2], [3, 4], :*) )
  end

  def test_dot_product
    nn = setup()
    assert_equal(11, nn.dot_product([1, 2], [3, 4]) )
    assert_equal(5, nn.dot_product([1, -2], [-3, -4]) )
  end

  def test_transpose
    nn = setup()
    assert_equal([[1, 3], [2, 4]], nn.transpose([[1, 2], [3, 4]]) )
  end

  def test_seed_produces_consistent_results
    nn = setup()
    nn.learn(quiet = true)
    assert_equal(0.001570932055395059, nn.error)
    assert_equal(-4.52646638889414, nn.bias)
    assert_equal([-0.41928280728738776, 10.863091157047998, -0.7927020934897735], nn.weights)
    assert_equal(0.0032094173531082475, nn.predict([1, 0, 1]))
    assert_equal(0, nn.predict_binary([1, 0, 1]))
  end

end
